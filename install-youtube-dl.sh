#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo apt install -y python mpv python3-pyxattr rtmpdump

sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl

sudo chmod a+rx /usr/local/bin/youtube-dl

# Verify File

sudo wget https://yt-dl.org/downloads/latest/youtube-dl.sig -O youtube-dl.sig

gpg --verify youtube-dl.sig /usr/local/bin/youtube-dl

rm youtube-dl.sig
