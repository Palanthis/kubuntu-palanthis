#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Kernel Build
sudo apt install linux-headers-$(uname -r) build-essential dkms

# Apps
sudo apt install conky qterminal cairo-dock geany screenfetch

# Call common script
sh ../003-common-script.sh
