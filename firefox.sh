#!/bin/bash
# Written by Palanthis
set -e
 
# Remove the snap, because f**k that.
sudo snap remove --purge firefox

# Add the mozilla PPA
sudo add-apt-repository ppa:mozillateam/ppa

# Create config to increase priority
sudo cp fun-stuff/mozillateamppa /etc/apt/preferences.d/

# Update package cache
sudo apt update

# Install Firefox
sudo apt install firefox
