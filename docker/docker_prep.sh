#!/bin/bash
# Written by Palanthis
set -e
 
sudo apt remove docker-desktop

rm -r $HOME/.docker/desktop

sudo rm /usr/local/bin/com.docker.cli

sudo apt purge docker-desktop
