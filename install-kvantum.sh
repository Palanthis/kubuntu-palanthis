#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo apt install g++ cmake libx11-dev libxext-dev qtbase5-dev libqt5svg5-dev libqt5x11extras5-dev libqt4-dev qttools5-dev-tools libkf5windowsystem-dev git

mkdir -p ~/repos/tsujan && cd ~/repos/tsujan
git clone https://github.com/tsujan/Kvantum.git && cd Kvantum
git checkout master

cd Kvantum
mkdir build && cd build
cmake ..
make

sudo make install

echo "export QT_STYLE_OVERRIDE=kvantum" >> ~/.profile
