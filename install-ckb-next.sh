#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo apt-get install libudev-dev qt5-default zlib1g-dev libappindicator-dev libpulse-dev libquazip5-dev
git clone https://github.com/ckb-next/ckb-next.git
cd ckb-next
./quickinstall
